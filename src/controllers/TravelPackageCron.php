<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use Hestec\TravelPackage\TravelPackage;
use SilverStripe\Core\Config\Config;
use Hestec\TravelPackage\CronLog;
use Hestec\TravelPackage\ProductFeed;
use Hestec\TravelPackage\Category;

class TravelPackageCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateDaisycon',
        'UpdateTradetracker',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {


            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://daisycon.io/datafeed/?filter_id=78013&settings_id=6865&records=3', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = $msgs = json_decode($response->getBody());


            foreach ($msgs->datafeed->programs as $node) {

                echo $node->program_info->name;

                foreach ($node->products as $travel) {

                    echo $travel->update_info->daisycon_unique_id."<br>";

                }

            }

            echo $response->getHeader('x-next-url')[0];

            return "test";

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateTravelPackage() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $this->UpdateDaisycon();
            //$this->AwinProgram();
            //$this->AwinSales('transaction');
            //$this->AwinSales('validation');

            $runtime = new \DateTime();
            $log = CronLog::get()->first();
            $log->LastRun = $runtime->format('Y-m-d H:i:s');
            $log->write();

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function UpdateDaisycon()
    {

        $lastrun = CronLog::get()->filter('AffiliateNetwork', 'Daisycon')->first();
        $current = new \DateTime();
        $last = new \DateTime($lastrun->LastRun);
        $current->modify('- 6 hours');

        if ($current > $last){

            $runtime = new \DateTime();
            $log = CronLog::get()->filter('AffiliateNetwork', 'Daisycon')->first();
            $log->LastRun = $runtime->format('Y-m-d H:i:s');
            $log->write();

            DB::query("UPDATE TravelPackageProductFeed SET Completed = 0, NextUrl = NULL WHERE AffiliateNetwork = 'Daisycon'");

        }

        $catlastminute = Category::get()->filter('SystemName', 'LASTMINUTE')->first();

        if ($feed = ProductFeed::get()->filter(array('Enabled' => true, 'AffiliateNetwork' => 'Daisycon', 'Completed' => false))->sort('"FeedType" ASC, "ID" ASC')->first()) {
            $feedurl = $feed->Url;
            if (filter_var($feed->NextUrl, FILTER_VALIDATE_URL)) {

                $feedurl = $feed->NextUrl;

            }

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', $feedurl . '&records=1000', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                //$email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                if (isset($response->getHeader('x-next-url')[0]) && filter_var($response->getHeader('x-next-url')[0], FILTER_VALIDATE_URL)) {

                    $feed->NextUrl = $response->getHeader('x-next-url')[0];
                    $feed->write();

                } else {

                    $feed->NextUrl = '';
                    $feed->Completed = true;
                    $feed->write();

                }

                $lastrun = CronLog::get()->first();
                $lastrun = new \DateTime($lastrun->LastRun);
                $lastrun->modify('-6 hours');

                $msgs = $msgs = json_decode($response->getBody());

                DB::query("UPDATE TravelPackage SET StillInApi = 0 WHERE DataFeed = '".$feed->Url."'");

                if ($msgs->datafeed->info->sub_category == "Accommodations & Transport") {

                    foreach ($msgs->datafeed->programs as $node) {

                        foreach ($node->products as $travel) {

                            $count = TravelPackage::get()->filter('BaseId', $travel->update_info->daisycon_unique_id)->count();

                            if ($travel->update_info->status == "active") {

                                if ($count == 0) {

                                    $trav = new TravelPackage();

                                } else {

                                    $trav = TravelPackage::get()->filter('BaseId', $travel->update_info->daisycon_unique_id)->first();
                                    $trav->categories()->removeAll();

                                }

                                foreach ($feed->categories() as $category) {

                                    $trav->categories()->add($category);

                                }

                                if (isset($travel->product_info->trip_lastminute) && $travel->product_info->trip_lastminute == "true") {

                                    $trav->categories()->add($catlastminute);

                                }

                                $trav->StillInApi = 1; // see the DB::query above
                                $trav->SupplierID = $feed->SupplierID;
                                $trav->DataFeed = $feed->Url;
                                $trav->BaseId = $travel->update_info->daisycon_unique_id;
                                $trav->Sku = $travel->product_info->sku;
                                $trav->Operator = $node->program_info->name;
                                $trav->Category = $travel->product_info->category;
                                $trav->Title = $travel->product_info->title;
                                $trav->Description = $travel->product_info->description;
                                $trav->Price = $travel->product_info->price;
                                $trav->PriceOld = $travel->product_info->price_old;
                                if ($trav->Price > 0 && $trav->PriceOld > $trav->Price) {
                                    $trav->DiscountPercent = $this->DiscountPercent($trav->PriceOld, $trav->Price);
                                }
                                $trav->BookUrl = $travel->product_info->link;
                                $trav->AffiliateNetwork = "Daisycon";

                                // only feedtype "Accommodations & Transport"
                                $trav->Country = $travel->product_info->destination_country;
                                $trav->AccommodationType = $travel->product_info->accommodation_type;
                                $trav->AccommodationName = $travel->product_info->accommodation_name;
                                $trav->MaxPeople = $travel->product_info->max_nr_people;
                                $trav->DepartureDate = $travel->product_info->departure_date;
                                $trav->DurationDays = $travel->product_info->duration_days;
                                $trav->DurationNights = $travel->product_info->duration_nights;
                                $trav->Rating = $travel->product_info->star_rating;
                                $trav->Latitude = $travel->product_info->destination_latitude;
                                $trav->Longitude = $travel->product_info->destination_longitude;
                                $trav->Region = $travel->product_info->destination_region;
                                $trav->TransportationType = $travel->product_info->travel_transportation_type;
                                $trav->AirportcodeDeparture = $travel->product_info->airportcode_departure;
                                $trav->AirportcodeDestination = $travel->product_info->airportcode_destination;
                                // end of only feedtype "Accommodations & Transport"

                                foreach ($travel->product_info->images as $image) {

                                    if ($image->type == "Default image for the product") {

                                        $trav->ImageURL = $image->location;
                                        break;

                                    }

                                }

                                $trav->write();

                                /*if ($travel->update_info->update_date > $lastrun->format('Y-m-d H:i:s') || $count == 0) {

                                    $imgitems = TravelPackageImage::get()->filter('TravelPackageID', $trav->ID);

                                    foreach ($imgitems as $item) {

                                        $item->delete();

                                    }

                                    foreach ($travel->product_info->images as $image) {

                                        $img = new TravelPackageImage();
                                        $img->TravelPackageID = $trav->ID;
                                        $img->Url = $image->location;
                                        $img->Type = $image->type;
                                        $img->write();
                                    }

                                }*/

                            }

                            //echo $travel->product_info->title."<br>";

                        }

                    }
                }
                if ($msgs->datafeed->info->sub_category == "Daily offers") {

                    $catdaydeal = Category::get()->filter('SystemName', 'DAYDEAL')->first();

                    foreach ($msgs->datafeed->programs as $node) {

                        foreach ($node->products as $travel) {

                            if ($trav = TravelPackage::get()->filter(array('SupplierID' => $feed->SupplierID, 'Sku' => $travel->product_info->sku, 'Price' => $travel->product_info->price))->first()) {

                                $trav->categories()->add($catdaydeal);
                                $trav->write();

                            }

                        }

                    }

                }

            }

            return "updated";

        }else{

            return "all feeds completed";

        }

    }

    public function UpdateTradetracker()
    {

        $lastrun = CronLog::get()->filter('AffiliateNetwork', 'Tradetracker')->first();
        $current = new \DateTime();
        $last = new \DateTime($lastrun->LastRun);
        $current->modify('- 6 hours');

        if ($current > $last){

            $runtime = new \DateTime();
            $log = CronLog::get()->filter('AffiliateNetwork', 'Tradetracker')->first();
            $log->LastRun = $runtime->format('Y-m-d H:i:s');
            $log->write();

            DB::query("UPDATE TravelPackageProductFeed SET Completed = 0, NextUrl = NULL WHERE AffiliateNetwork = 'Daisycon'");

        }

        if ($feed = ProductFeed::get()->filter(array('AffiliateNetwork' => 'Tradetracker', 'Completed' => false))->sort('ID')->first()) {
            $feedurl = $feed->Url;
            if (filter_var($feed->NextUrl, FILTER_VALIDATE_URL)) {

                $feedurl = $feed->NextUrl;

            }

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', $feedurl . '&records=1000', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Energy", $e->getMessage());
                //$email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                if (isset($response->getHeader('x-next-url')[0]) && filter_var($response->getHeader('x-next-url')[0], FILTER_VALIDATE_URL)) {

                    $feed->NextUrl = $response->getHeader('x-next-url')[0];
                    $feed->write();

                } else {

                    $feed->NextUrl = '';
                    $feed->Completed = true;
                    $feed->write();

                }

                $lastrun = CronLog::get()->first();
                $lastrun = new \DateTime($lastrun->LastRun);
                $lastrun->modify('-6 hours');

                $msgs = $msgs = json_decode($response->getBody());

                DB::query("UPDATE TravelPackage SET StillInApi = 0");

                foreach ($msgs->datafeed->programs as $node) {

                    foreach ($node->products as $travel) {

                        $count = TravelPackage::get()->filter('BaseId', $travel->update_info->daisycon_unique_id)->count();

                        if ($travel->update_info->status == "active") {

                            if ($count == 0) {

                                $trav = new TravelPackage();

                            } else {

                                $trav = TravelPackage::get()->filter('BaseId', $travel->update_info->daisycon_unique_id)->first();

                            }

                            $trav->StillInApi = 1; // see the DB::query above
                            $trav->BaseId = $travel->update_info->daisycon_unique_id;
                            $trav->Supplier = $node->program_info->name;
                            $trav->Operator = $node->program_info->name;
                            $trav->Category = $travel->product_info->category;
                            $trav->LastMinute = $travel->product_info->trip_lastminute;
                            $trav->Country = $travel->product_info->destination_country;
                            $trav->Title = $travel->product_info->title;
                            $trav->Description = $travel->product_info->description;
                            $trav->AccommodationType = $travel->product_info->accommodation_type;
                            $trav->AccommodationName = $travel->product_info->accommodation_name;
                            $trav->MaxPeople = $travel->product_info->max_nr_people;
                            $trav->DepartureDate = $travel->product_info->departure_date;
                            $trav->DurationDays = $travel->product_info->duration_days;
                            $trav->DurationNights = $travel->product_info->duration_nights;
                            $trav->Rating = $travel->product_info->star_rating;
                            $trav->Latitude = $travel->product_info->destination_latitude;
                            $trav->Longitude = $travel->product_info->destination_longitude;
                            $trav->Price = $travel->product_info->price;
                            $trav->PriceOld = $travel->product_info->price_old;
                            if ($trav->Price > 0 && $trav->PriceOld > $trav->Price){
                                $trav->DiscountPercent = $this->DiscountPercent($trav->PriceOld, $trav->Price);
                            }
                            $trav->Region = $travel->product_info->destination_region;
                            $trav->TransportationType = $travel->product_info->travel_transportation_type;
                            $trav->AirportcodeDeparture = $travel->product_info->airportcode_departure;
                            $trav->AirportcodeDestination = $travel->product_info->airportcode_destination;
                            $trav->BookUrl = $travel->product_info->link;

                            foreach ($travel->product_info->images as $image) {

                                if ($image->type == "Default image for the product"){

                                    $trav->ImageURL = $image->location;
                                    break;

                                }

                            }

                            $trav->write();

                            /*if ($travel->update_info->update_date > $lastrun->format('Y-m-d H:i:s') || $count == 0) {

                                $imgitems = TravelPackageImage::get()->filter('TravelPackageID', $trav->ID);

                                foreach ($imgitems as $item) {

                                    $item->delete();

                                }

                                foreach ($travel->product_info->images as $image) {

                                    $img = new TravelPackageImage();
                                    $img->TravelPackageID = $trav->ID;
                                    $img->Url = $image->location;
                                    $img->Type = $image->type;
                                    $img->write();
                                }

                            }*/

                        }

                        //echo $travel->product_info->title."<br>";

                    }

                }
            }

            return "updated";

        }else{

            return "all feeds completed";

        }

    }

    public function DiscountPercent($oldPrice, $newPrice){

        $decreaseValue = $oldPrice - $newPrice;

        return ($decreaseValue / $oldPrice) * 100;
    }

}
