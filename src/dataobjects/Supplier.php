<?php

namespace Hestec\TravelPackage;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\RequiredFields;

class Supplier extends DataObject {

    private static $singular_name = 'Supplier';
    private static $plural_name = 'Suppliers';

    private static $table_name = 'TravelPackageSupplier';

    private static $db = array(
        'Name' => 'Varchar(100)'
    );

    private static $has_many = array(
        'TravelPackages' => TravelPackage::class
    );

    private static $summary_fields = array(
        'Name'
    );

    public function getCMSFields() {

        $NameField = TextField::create('Name', 'Name');

        return new FieldList(
            $NameField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'Name'
        ));
    }

}