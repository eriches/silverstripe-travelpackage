<?php

namespace Hestec\TravelPackage;

use SilverStripe\ORM\DataObject;

class CronLog extends DataObject {

    private static $table_name = 'TravelPackageCronLog';

    private static $db = [
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,TradeDoubler','')",
        'LastRun' => 'Datetime'
    ];

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (CronLog::get()->filter('AffiliateNetwork', 'Daisycon')->count() == 0){

            $runtime = new \DateTime();
            $record = new CronLog();
            $record->AffiliateNetwork = "Daisycon";
            $record->LastRun = $runtime->format('Y-m-d');
            $record->write();

        }
        if (CronLog::get()->filter('AffiliateNetwork', 'Awin')->count() == 0){

            $runtime = new \DateTime();
            $record = new CronLog();
            $record->AffiliateNetwork = "Awin";
            $record->LastRun = $runtime->format('Y-m-d');
            $record->write();

        }
        if (CronLog::get()->filter('AffiliateNetwork', 'TradeTracker')->count() == 0){

            $runtime = new \DateTime();
            $record = new CronLog();
            $record->AffiliateNetwork = "TradeTracker";
            $record->LastRun = $runtime->format('Y-m-d');
            $record->write();

        }
        if (CronLog::get()->filter('AffiliateNetwork', 'TradeDoubler')->count() == 0){

            $runtime = new \DateTime();
            $record = new CronLog();
            $record->AffiliateNetwork = "TradeDoubler";
            $record->LastRun = $runtime->format('Y-m-d');
            $record->write();

        }

    }

}