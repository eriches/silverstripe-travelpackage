<?php

namespace Hestec\TravelPackage;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CheckboxSetField;

class ProductFeed extends DataObject {

    private static $table_name = 'TravelPackageProductFeed';

    private static $db = [
        'Enabled' => 'Boolean',
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,TradeDoubler','')",
        'Name' => 'Varchar(255)',
        'Url' => 'Varchar(255)',
        'NextUrl' => 'Varchar(255)',
        'UpdateType' => "Enum('ALL,PART','ALL')", // ALL = get all and use stillinapi - PART = per update: insert, update, delete
        'FeedType' => 'Int',
        'Completed' => 'Boolean'
    ];

    private static $defaults = array(
        'Enabled' => true,
        'FeedType' => 1
    );

    private static $default_sort='ID';

    private static $has_one = array(
        'Supplier'=> Supplier::class
    );

    private static $many_many = array(
        'Categories'=> Category::class
    );

    private static $summary_fields = array(
        'Name',
        'AffiliateNetwork',
        'Url',
        'FeedType',
        'Enabled'
    );

    public function getCMSFields() {

        $UrlField = TextField::create('Url', 'Url');
        $NameField = TextField::create('Name', 'Name');
        $AffiliateNetworkField = DropdownField::create('AffiliateNetwork', 'AffiliateNetwork', $this->dbObject('AffiliateNetwork')->enumValues());
        $AffiliateNetworkField->setEmptyString("(select)");
        $UpdateTypeField = DropdownField::create('UpdateType', 'UpdateType', $this->dbObject('UpdateType')->enumValues());
        $UpdateTypeField->setDescription("alleen voor Daisycon, voor groot aantal records kies PART.");
        $EnabledField = CheckboxField::create('Enabled', "Enabled");

        $CategoriesSource = Category::get()->map('ID', 'DutchTitle')->toArray();
        $CategoriesField = CheckboxSetField::create('Categories', 'Categories', $CategoriesSource);

        $FeedTypeSource = array(
            1 => "Accommodations & Transport",
            2 => "Day Deal"
        );
        $FeedTypeField = DropdownField::create('FeedType', 'FeedType', $FeedTypeSource);
        $FeedTypeField->setDescription("DAYDEAL voegt alleen de category DAYDEAL toe aan een bestaabd record.");

        $SupplierSource = Supplier::get();
        $SupplierField = DropdownField::create('SupplierID', 'Supplier', $SupplierSource);
        $SupplierField->setEmptyString("(select)");

        return new FieldList(
            $EnabledField,
            $AffiliateNetworkField,
            $SupplierField,
            $NameField,
            $UrlField,
            $FeedTypeField,
            $UpdateTypeField,
            $CategoriesField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'AffiliateNetwork',
            'Url',
            'Name',
            'FeedType',
            'SupplierID'
        ));
    }

    public function validate()
    {
        $result = parent::validate();

        if (!filter_var($this->Url, FILTER_VALIDATE_URL)){
            $result->addError('This is not a valid url.');
        }

        return $result;
    }

}