<?php

namespace Hestec\TravelPackage;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\RequiredFields;

class Category extends DataObject {

    private static $singular_name = 'Category';
    private static $plural_name = 'Categories';

    private static $table_name = 'TravelPackageCategory';

    private static $db = array(
        'SystemName' => 'Varchar(100)',
        'DutchTitle' => 'Varchar(100)',
    );

    private static $belongs_many_many = array(
        'TravelPackages' => TravelPackage::class,
        'ProductFeeds' => ProductFeed::class,
    );

    private static $summary_fields = array(
        'SystemName',
        'DutchTitle'
    );

    public function getCMSFields() {

        $SystemNameField = TextField::create('SystemName', 'SystemName');
        $DutchTitleField = TextField::create('DutchTitle', 'DutchTitle');

        return new FieldList(
            $SystemNameField,
            $DutchTitleField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'SystemName',
            'DutchTitle'
        ));
    }

    function requireDefaultRecords(){
        parent::requireDefaultRecords();

        if (Category::get()->filter('SystemName', 'LASTMINUTE')->count() == 0){

            $record = new Category();
            $record->SystemName = "LASTMINUTE";
            $record->DutchTitle = "last minute";
            $record->write();

        }
        if (Category::get()->filter('SystemName', 'DAYDEAL')->count() == 0){

            $record = new Category();
            $record->SystemName = "DAYDEAL";
            $record->DutchTitle = "dagaanbieding";
            $record->write();

        }

    }

}