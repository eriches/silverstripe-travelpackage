<?php

namespace Hestec\TravelPackage;

use SilverStripe\ORM\DataObject;
use SilverStripe\CMS\Model\SiteTree;

class TravelPackage extends DataObject {

    private static $singular_name = 'TravelPackage';
    private static $plural_name = 'TravelPackages';

    private static $table_name = 'TravelPackage';

    private static $db = array(
        'BaseId' => 'Varchar(255)',
        'Sku' => 'Varchar(50)',
        'Operator' => 'Varchar(255)',
        'Category' => 'Varchar(255)',
        'Country' => 'Varchar(2)',
        'Region' => 'Varchar(255)',
        'Title' => 'Varchar(255)',
        'Description' => 'Text',
        'AccommodationType' => 'Varchar(255)',
        'AccommodationName' => 'Varchar(255)',
        'MaxPeople' => 'Int',
        'DepartureDate' => 'Date',
        'DurationDays' => 'Int',
        'DurationNights' => 'Int',
        'Rating' => 'Int',
        'Latitude' => 'Varchar(20)',
        'Longitude' => 'Varchar(20)',
        'Price' => 'Currency',
        'PriceOld' => 'Currency',
        'DiscountPercent' => 'Decimal',
        'ImageURL' => 'Varchar(255)',
        'TransportationType' => 'Varchar(50)',
        'AirportcodeDeparture' => 'Varchar(3)',
        'AirportcodeDestination' => 'Varchar(3)',
        'BookUrl' => 'Varchar(255)',
        'AffiliateNetwork' => "Enum('Daisycon,Awin,TradeTracker,TradeDoubler','')",
        'StillInApi' => 'Boolean',
        'DataFeed' => 'Varchar(255)'
    );

    private static $has_one = array(
        'Supplier'=> Supplier::class
    );

    private static $many_many = array(
        'Categories'=> Category::class
    );

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return "€ ".$output;

    }

    public function RoundedDiscountPercent(){

        return round($this->DiscountPercent);

    }

    public function LinkString(){

        return SiteTree::create()->generateURLSegment($this->Title);

    }

    public function StrippedDescription(){

        return strip_tags($this->Description);

    }

    /*public function onBeforeWrite()
    {

        if ($this->ID && $this->isChanged('UrlId')){

            $oldurl = WlabelEnergySubscription::get()->byID($this->ID)->UrlId;

            $add = new WlabelEnergyOldUrl();
            $add->UrlId = $oldurl;
            $add->WlabelEnergySubscriptionID = $this->ID;
            $add->write();

        }

        parent::onBeforeWrite();

    }*/

}